jQuery(document).ready( function(){
	
	
	function runAjax( funcName, funcArgs ){
		jQuery.ajax({
			url	:	"/wp-content/plugins/WPHPMyAdmin/ajax-functions.php",
			type	:	"POST",
			data	:	{ func: funcName.toString(), args: funcArgs.toString() }
		}).success(function( msg ){
		
			var html = msg;
			
			jQuery( html ).insertBefore( "#submit" );
			
			console.log("jQuery Ajax Success");
			
		}).error(function( jqXHR, textStatus ){
			
			var html = "<div class='toRemove'><p>There Was An Error</p><p>Please Refresh the Page</p></div>";
			
			jQuery( html ).insertBefore( "#submit" );
			
			console.log("REQUEST FAILED: " + textStatus);
			
		});
	}
	
	
	jQuery(document).on("click", "#wphpmyadmin td, #wphpmyadmin th, #wphpmyadmin-menu a, #sqlsubmit", function(evt){
		
		
		
		var funcName = ( typeof this.dataset == "object") ? this.dataset.func : this.getAttribute( "data-func" );
		var args = (typeof this.dataset == "object") ? this.dataset.args : this.getAttribute( "data-args" );
		
		if( args.indexOf("javascript:") != -1 ){
			args = eval( args.substr(11) );
		}
		
		jQuery("#wphpmyadmin").remove();
		jQuery("#sql-editor-form").remove();
		jQuery(".toRemove").remove();
		
		runAjax( funcName, args );
		
	});
	
	
	
});
