<?php
	
	require( $_SERVER['DOCUMENT_ROOT'] . '/wp-blog-header.php'); // This limits the plugin to only being used on wp sites that are installed to docroot. Needs FIXED!
	
	global $wpdb;
	
	$post = $_POST;
	
	global $current_user;
	
	
	// Function returning whether user has role of administrator. 
	function isAdmin(){
		global $current_user;
		
		$roles = $current_user->roles;
		
		foreach($roles as $role){
			
			if( strcmp($role,"administrator") == 0 ){
				return true;
			}

		}
		
		return false;
	}
	
	if( !isAdmin() ){
		
		echo "<h2>Not Allowed To Access This!</h2>";
		die;
		
	}
	
	function showTable( $tbl ){
		
		global $wpdb;
		
		//$suppress = $wpdb->suppress_errors();
		
		$headers = $wpdb->get_results( "DESCRIBE $tbl" );
		$rows = $wpdb->get_results( "SELECT * FROM $tbl LIMIT 0, 30;" ); // This will eventually have pagination
		
		//$wpdb->suppress_errors( $suppress );
		
		$colnames = [];
		
		//var_dump( $headers );
		$html = "<table class='res-table' id='wphpmyadmin'>";
		$html .= "<thead><tr>";
		foreach( $headers as $header => $col ){
			
			$colnames[] = $col->Field;
			
			$html .= "<th>$col->Field</th>";
		}
		
		//var_dump( $colnames );
		
		$html .= "</tr></thead><tbody>";
		
		$alternateRow = false;
		
		foreach( $rows as $row => $col){
			
			$alternateRow = !$alternateRow;
			
			$html .= ($alternateRow) ? "<tr class='odd'>" : "<tr class='even'>";
			
			
			
			for($i=0;$i<sizeof($colnames);$i++){
				$html .= "<td>". substr($col->{$colnames[$i]}, 0, 50) ."</td>";
			}
			
			$html .= "</tr>";
			
			//var_dump( $col );
			
		}
		$html .= "</tbody></table>";
		echo $html;
		
	}
	
	function listTables(){
		
		global $wpdb;
		
		$suppress = $wpdb->suppress_errors();
		
		$tables = $wpdb->get_results( "SHOW Tables" );
		
		$wpdb->suppress_errors( $suppress );
		
		$alt = false;
		
		$html = "<table id='wphpmyadmin'>";
		$html .= "<thead><th data-func='listTables'>Tables</th></thead><tbody>";
		
		foreach( $tables as $index => $table ){
						
			$alt = !$alt;
						
			$html .= ($alt) ? "<tr class='odd'>" : "<tr class='even'>";
			$html .= "<td data-func='showTable' data-args='{$table->Tables_in_wordpress}'>{$table->Tables_in_wordpress}</td></tr>";
						
		}
		
		$html .= "</tbody></table>";
		
		echo $html;
	
	}
	
	function SQL(){
		
		$html = "<form id='sql-editor-form' action='#' name='sql-editor-form' method='post'><textarea id='sqleditor' class='sqleditor'></textarea>";
		$html .= "<input type='button' value='Run SQL' id='sqlsubmit' class='button-primary' data-func='query' data-args='javascript:jQuery(\"#sqleditor\").val()'/></form>";
		
		echo $html;
	}
	
	function query( $sql ){
		
		global $wpdb;
		$html = "<table id='wphpmyadmin'>";
		$html .= "<thead>";
		
		try{
		
			$suppress = $wpdb->suppress_errors();
			$res = $wpdb->get_results( $sql );
			$wpdb->suppress_errors( $suppress );
			
			
			$colnames = get_object_vars( $res[0] ); // I'M NOT SURE IF THIS IS OKAY. PLEASE FIND A BETTER WAY TO DO IT, IF POSSIBLE.
			
			var_dump( $colnames );
			
			$html .= "<tr>";
						
			foreach( $colnames as $colname ){
				$html .= "<th>$colnames[$colname]</th>";
			}
			
			$html .= "</tr></thead>";
			$html .= "<tbody>";
			
			
			foreach( $res as $row => $obj ){
				
				$html .= "<tr>";
				
				foreach( $colnames as $colname ){
					
					$html .= "<td>{$obj->$colname}</td>";
					
				}
				
				$html .= "</tr>";
				
			}
			
			$html .= "</tbody></table>";
			
			echo $html;
			
		} catch( Exception $exception ) {
			
			echo "<h2>Error Found!</h2>";
			
		}
		
	}
	
	//handler( $post['func'], $post['table'] );
	
	$post['func']( $post['args'] );
	// $post['func'] returns a function reference, and the ($post['table']) returns the arguments, so it turns into a function call.
	
?>
