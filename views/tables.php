<!DOCTYPE html>
<html>
	<body>
		<table id="php-my-admin-wp">
			<thead>
				<th class='listTbl'>Tables</th>
			</thead>
			<tbody>
				<?php 
				
					// $tables RESULT FROM THE $wpdb QUERY FOR TABLES 
					// $table IS EQUIVALENT TO $tables[$index]
					// USE $colname IN REFERENCING TABLE: $table->$colname
					foreach( $tables as $index => $table ){
						
						echo "<tr><td class='showTbl'>{$table->Tables_in_wordpress}</td></tr>";
						
					}
				
				?>
			</tbody>
		</table>
	</body>
</html>

