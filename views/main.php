<div class="wrap">

	<a href='/wp-admin/admin.php?page=option-editor'><div id="icon-tools" class="icon32"></div></a>
	
	<h2>
		<?php echo bloginfo('name'), " Database Editor"; ?>
	</h2>

	<?php if (isset($message)) { ?>
		<div class="updated"><?php echo $message; ?></div>
	<?php unset($message); } ?>
	
	<form method="post" action="<?php echo $_SERVER['REQUEST_URI']; ?>" id='php-my-admin-form-wp'>
		
		<!--<input type="hidden" name="step" value="1" />-->
		
		<div class='nav'>
			<ul id='wphpmyadmin-menu'>
				<li><a href='#' data-func='listTables' data-args="">Main</a></li>
				<li><a href='#' data-func='SQL' data-args="">SQL</a></li>
			</ul>
		</div>
		
		<table id="wphpmyadmin">
			<thead>
				<th data-func='listTables' data-args="">Tables</th>
			</thead>
			<tbody>
				<?php 
					$alt = false;
					// $tables RESULT FROM THE $wpdb QUERY FOR TABLES 
					// $table IS EQUIVALENT TO $tables[$index]
					// USE $colname IN REFERENCING TABLE: $table->$colname
					foreach( $tables as $index => $table ){
						
						$alt = !$alt;
						
						echo ($alt) ? "<tr class='odd'>" : "<tr class='even'>" , "<td data-func='showTable' data-args='{$table->Tables_in_wordpress}'>{$table->Tables_in_wordpress}</td></tr>";
						
					}
				
				?>
			</tbody>
		</table>
		
		
		
		<p class="submit" id='submit'>
			<input class="button-primary" type="submit" value="Edit" id="submitbutton" />
		</p>
	
	</form>
