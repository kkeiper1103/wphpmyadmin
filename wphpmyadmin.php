<?php
/*
Plugin Name: WPHPMyAdmin
Description: Edit the Current Installation's Database. Loosely based on PHPMyAdmin
Author: Kyle Keiper
Version: 0.4.1
*/
define("OPT_DIR",dirname(__FILE__));

	/**
	 * 
	 * @author Kyle Keiper
	 * 
	 * @license New BSD License
	 * @copyright All rights reserved Kyle Keiper 2012
	 */

class OptionEditorPlugin {
	
	public function __construct(){
		
		add_action("admin_menu", array($this,"admin_menu"));
		add_action("admin_init", array($this,"admin_init"));
		
	}
	
	public function admin_init(){
		
		wp_enqueue_script("wphpmyadmin-js",plugins_url('/js/wphpmyadmin.js',__FILE__));
		wp_enqueue_style("wphpmyadmin-css", plugins_url('/css/wphpmyadmin.css', __FILE__) );
	}
	
	public function admin_menu(){
		
		add_menu_page('W-PHPMyAdmin','W-PHPMyAdmin', 'manage_options','wphpmyadmin',array($this,"plugin_page"), NULL);
	
	}
	
	public function show_tables(){
		
		global $wpdb;
		
		$suppress = $wpdb->suppress_errors();
		
		$tables = $wpdb->get_results( "SHOW Tables" );
		
		$wpdb->suppress_errors( $suppress );
		
		return $tables;
		
	}
	
	public function plugin_page(){
			
		$tables = $this->show_tables();	
		$pageFile = '/views/main.php';
		
		require_once OPT_DIR.$pageFile;
	}
	
}

$optionEditorPlugin = new OptionEditorPlugin();
